import React, { useCallback, useEffect, useState } from 'react';
import { Button, Modal, ScrollView, Image, StyleSheet, Text, View, TextInput, Alert, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import startFirebase from './config/FIREBASE';
import { ref, push, set, get, update, remove, child } from 'firebase/database';
import DocumentPicker from 'react-native-document-picker';
import { getStorage, ref as sRef, uploadBytes, getDownloadURL, deleteObject } from 'firebase/storage'

const ProductAdd = ( {route, navigation} ) => { 
    const { productSelected } = route.params;

    const [modalVisible, setModalVisible] = useState(false)
    const [tipeKelola, settipeKelola] = useState('tambah')
    const [jumlahInject, setjumlahInject] = useState('0')

    const ubahTipeKelola = (tipe) => {
      setjumlahInject('0');
      settipeKelola(tipe);
    }

    const handleStokInput = (jumlah_stok) => {
      if (jumlah_stok == '') {
        setjumlahInject('0');
      } else {
        if (tipeKelola == 'kurang') {
          if (parseFloat(jumlah_stok) > parseFloat(productSelected.stok)) {
            setjumlahInject(productSelected.stok);
          } else {
            jumlah_stok = parseFloat(jumlah_stok);

            setjumlahInject('' + jumlah_stok);
          }
        } else {
          jumlah_stok = parseFloat(jumlah_stok);

          setjumlahInject('' + jumlah_stok);
        }
      }
    }

    const updateStok = () => {
      let tipe = tipeKelola;
      let stok_terkini = productSelected.stok;
      let jumlah_stok = jumlahInject;

      if (!tipe || !jumlah_stok) {
        Alert.alert(
          'Data stok belum lengkap!'
        )
        return false;
      }

      let new_stok = stok_terkini;

      if (tipe == 'tambah') {
        new_stok = parseFloat(stok_terkini) + parseFloat(jumlah_stok);
      }

      if (tipe == 'kurang') {
        new_stok = parseFloat(stok_terkini) - parseFloat(jumlah_stok);
      }

      setModalVisible(true);

      const db = startFirebase();

      update(ref(db, 'Product/'+productSelected.id), {
        stok: '' + new_stok
      })
      .then(() => {
          setModalVisible(false);

          const updated_product = {...productSelected, stok: '' + new_stok};

          console.log(updated_product);

          Alert.alert(
              '',
              'berhasil',
              [
                  {
                      text: 'OK',
                      onPress: () => navigation.navigate('Edit Product', {type: 'edit', product: updated_product})
                  }
              ]
          )
      })
      .catch((error) => {
          setModalVisible(false);
          Alert.alert(
              '',
              'gagal'
          )
      });
    }

    return (
        <View style={{alignItems: 'center', flex: 1, backgroundColor: '#cccccc'}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                Alert.alert('Modal has been closed.');
                setModalVisible(!modalVisible);
                }}>
                <View style={styless.centeredView}>
                <View style={styless.modalView}>
                    <Text style={styless.modalText}>Loading...</Text>
                </View>
                </View>
            </Modal>
            <View style={{width: 320, paddingTop: 10, paddingBottom: 10}}>
                <Text style={{fontFamily: 'Poppins-Regular'}}>Tipe Managemen Stok</Text>
                <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                    <TouchableOpacity 
                    onPress={() => ubahTipeKelola('tambah')} 
                    style={{borderWidth: 1, marginBottom: 5, borderRadius: 2, justifyContent: 'center', backgroundColor: tipeKelola == 'tambah' ? '#666666' : 'white', width: '49%', height: 70}}
                    ><Text style={{color: tipeKelola == 'tambah' ? 'white' : 'black', alignSelf: 'center'}}><AntDesign name="plus" size={50} /></Text></TouchableOpacity>
                    <View style={{width: '2%'}}></View>
                    <TouchableOpacity 
                    onPress={() => ubahTipeKelola('kurang')} 
                    style={{borderWidth: 1, marginBottom: 5, borderRadius: 2, justifyContent: 'center', backgroundColor: tipeKelola == 'kurang' ? '#666666' : 'white', width: '49%', height: 70}}
                    ><Text style={{color: tipeKelola == 'kurang' ? 'white' : 'black', alignSelf: 'center'}}><AntDesign name="minus" size={50} /></Text></TouchableOpacity>
                </View>
                
                <Text style={{fontFamily: 'Poppins-Regular'}}>Stok Terkini</Text>
                <TextInput editable={false} keyboardType='numeric' placeholder='Jumlah stok terkini' style={styles.inputNorm} value={productSelected.stok}></TextInput>
                
                <Text style={{fontFamily: 'Poppins-Regular'}}>Jumlah Stok</Text>
                <TextInput keyboardType='numeric' placeholder='Jumlah stok yang akan diinject' style={styles.inputNorm} value={jumlahInject} onChangeText={newText => handleStokInput(newText)}></TextInput>

                <Button onPress={() => {updateStok()}} title='submit'></Button>
            </View>
        </View>
    );
}

export default ProductAdd;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular', 
        color: '#808080',
        alignSelf: 'center'
    },
    textnocenter: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular',
        color: '#42baf8'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    inputNorm: {
        borderWidth: 1,
        marginBottom: 10,
        borderRadius: 2,
        backgroundColor: 'white',
        fontFamily: 'Poppins-Regular'
    }
});

const styless = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 22,
    },
    modalView: {
      margin: 20,
      backgroundColor: 'white',
      borderRadius: 20,
      padding: 35,
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
    },
    buttonOpen: {
      backgroundColor: '#F194FF',
    },
    buttonClose: {
      backgroundColor: '#2196F3',
    },
    textStyle: {
      color: 'white',
      fontWeight: 'bold',
      textAlign: 'center',
    },
    modalText: {
      marginBottom: 15,
      textAlign: 'center',
    },
  });