import React, { useCallback, useEffect, useState } from 'react';
import { Button, ScrollView, Image, StyleSheet, Text, View, TextInput, Alert, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import startFirebase from './config/FIREBASE';
import { ref, push, set, get, update, remove, child, getKey } from 'firebase/database';
import DocumentPicker from 'react-native-document-picker';
import { getStorage, ref as sRef, uploadBytes, getDownloadURL, deleteObject } from 'firebase/storage'
import DropDownPicker from 'react-native-dropdown-picker';
import { useIsFocused } from '@react-navigation/native';

const Sale_detail = ( {route, navigation} ) => { 
    const isFocused = useIsFocused();
    const { sale } = route.params;
    const [saleDetail, setsaleDetail] = useState([])

    useEffect(() => {
      
    }, [sale])
    

    const getSalesDetail = () => {
        const db = startFirebase();
        const dbref = ref(db);

        get(child(dbref, 'Sales_detail/')).then((snapshot) => {
            if(snapshot.exists()) {
                let data = JSON.stringify(snapshot);
                data = JSON.parse(data);

                var arr = [];

                for (var prop in data) {
                    if (data.hasOwnProperty(prop)) {
                        var innerObj = {};
                        innerObj[prop] = data[prop];
                        arr.push(innerObj)
                    }
                }

                var arrfinal = [];

                arr.map(item => {
                    for (var prop in item) {
                        if (item.hasOwnProperty(prop)) {
                            item[prop].id = prop

                            arrfinal.push(item[prop])
                        }
                    }
                })

                var arrfinal1 = [];

                if (arrfinal.length > 0) {
                    arrfinal.map(item => {
                        if (item.sale_id == sale.id) {
                            let total = parseFloat(item.price) * parseFloat(item.qty);

                            let harga_formatted = new Intl.NumberFormat("id-ID", {
                                style: "currency",
                                currency: "IDR"
                              }).format(item.price);

                            let total_formatted = new Intl.NumberFormat("id-ID", {
                                style: "currency",
                                currency: "IDR"
                            }).format(total);
        
                            let harga = harga_formatted.split(',');
                            let total_display = total_formatted.split(',');
        
                            item.harga_display = harga[0];
                            item.total_display = total_display[0];

                            arrfinal1.push(item);
                        }
                    })
                }

                if (arrfinal1.length > 0) {
                    setsaleDetail(arrfinal1);
                } else {
                    setsaleDetail([]);
                }
            }
        });
    }

    useEffect(() => {
        getSalesDetail();
    }, [])

    const editSale = () => {
        let saleData = {
            ...sale,
            detail: saleDetail
        };

        navigation.navigate('Edit Sale', {type: 'edit', sale: saleData})
    }

    return (
        <View style={{alignItems: 'center', flex: 1}}>
            <View style={{width: 335, marginTop: 15, marginBottom: 15, borderRadius: 5, backgroundColor: '#cccccc', padding: 10}}>
                <View style={{width: '100%', flexDirection: 'row'}}>
                    <View style={{width: '80%'}}>
                        <Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>Tanggal penjualan: {sale.tanggal_aj}</Text>
                        <Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>Waktu penjualan: {sale.time}</Text>
                    </View>
                    <View style={{width: '20%'}}>
                        <TouchableOpacity onPress={() => editSale()} style={{backgroundColor: 'orange', borderRadius: 10, paddingVertical: 10, paddingHorizontal: 13, alignSelf: 'flex-end'}}><Text style={{color: 'white'}}><Icon name="edit" size={25} /></Text></TouchableOpacity>
                    </View>
                </View>
                <View style={{backgroundColor: 'white', borderRadius: 5, marginTop: 10}}>
                    <View style={{backgroundColor: '#33cc33', flexDirection: 'row'}}>
                        <View style={{borderWidth: 0.6, borderColor: 'black', width: '30%', padding: 2}}><Text style={{color: 'black', fontFamily: 'Poppins-Regular', fontSize: 13}}>Nama</Text></View>
                        <View style={{borderWidth: 0.6, borderColor: 'black', width: '23%', padding: 2}}><Text style={{color: 'black', fontFamily: 'Poppins-Regular', fontSize: 13}}>Harga @</Text></View>
                        <View style={{borderWidth: 0.6, borderColor: 'black', width: '19%', padding: 2}}><Text style={{color: 'black', fontFamily: 'Poppins-Regular', fontSize: 13}}>Jumlah</Text></View>
                        <View style={{borderWidth: 0.6, borderColor: 'black', width: '28%', padding: 2}}><Text style={{color: 'black', fontFamily: 'Poppins-Regular', fontSize: 13}}>Total</Text></View>
                    </View>
                    {
                        saleDetail.map((item, index) => 
                            <View key={index} style={{flexDirection: 'row'}}>
                                <View style={{borderWidth: 0.6, borderColor: 'black', width: '30%', padding: 2}}><Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>{item.name}</Text></View>
                                <View style={{borderWidth: 0.6, borderColor: 'black', width: '23%', padding: 2}}><Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>{item.harga_display}</Text></View>
                                <View style={{borderWidth: 0.6, borderColor: 'black', width: '19%', padding: 2}}><Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>{item.qty}</Text></View>
                                <View style={{borderWidth: 0.6, borderColor: 'black', width: '28%', padding: 2}}><Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>{item.total_display}</Text></View>
                            </View>
                        )
                    }
                    <View style={{backgroundColor: '#cccccc', flexDirection: 'row', paddingTop: 10}}>
                        <View style={{width: '30%'}}></View>
                        <View style={{width: '42%', padding: 2}}><Text style={{color: 'black', fontFamily: 'Poppins-Regular', fontSize: 13}}>Total penjualan :</Text></View>
                        <View style={{width: '28%', padding: 2}}><Text style={{color: 'black', fontFamily: 'Poppins-Regular', fontSize: 13}}>{sale.total}</Text></View>
                    </View>
                </View>
            </View>
        </View>
    );
}

export default Sale_detail;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular', 
        color: '#808080'
    },
    textnocenter: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular',
        color: '#42baf8'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    inputNorm: {
        borderWidth: 1,
        marginBottom: 10,
        borderRadius: 2,
        backgroundColor: 'white'
    }
});