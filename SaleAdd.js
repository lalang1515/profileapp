import React, { useCallback, useEffect, useState } from 'react';
import { TouchableOpacity, Modal, Button, ScrollView, Image, StyleSheet, Text, View, TextInput, Alert } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import AntIcon from 'react-native-vector-icons/AntDesign'
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import startFirebase from './config/FIREBASE';
import { ref, push, set, get, update, remove, child, query, orderByChild, equalTo } from 'firebase/database';
import DocumentPicker from 'react-native-document-picker';
import { getStorage, ref as sRef, uploadBytes, getDownloadURL, deleteObject } from 'firebase/storage'
import DropDownPicker from 'react-native-dropdown-picker';
import { useIsFocused } from '@react-navigation/native';

const SaleAdd = ( {route, navigation} ) => { 
    const isFocused = useIsFocused()
    const { type, sale } = route.params;

    useEffect(() => {
        if (type == 'edit') {
            getProductStockEdit(sale.detail);
        }
    }, [])

    const getProductStockEdit = (detail) => {
        const db = startFirebase();
        const dbref = ref(db);

        let finalDetail = [];

        detail.map(item => {
            get(child(dbref, 'Product/' + item.item_id)).then((snapshot) => {
                if(snapshot.exists()) {    
                    let data = JSON.stringify(snapshot);
                    data = JSON.parse(data);

                    let new_stok = parseFloat(data.stok) + parseFloat(item.qty);

                    finalDetail.push({id: item.item_id, name: item.name, price: item.price, qty: item.qty, stok: '' + new_stok});
                }
            });
        });

        setitems(finalDetail);
        getProduct(finalDetail);
    }

    const create = async () => {
        if (items.length == 0) {
            Alert.alert(
                'Belum ada produk yang ditambahkan'
            )
        } else {
            const db = startFirebase();
    
            const postListRef = ref(db, 'Sales');
    
            if (type == 'add') {
                const newPostRef = push(postListRef);

                const today = new Date();
                const yyyy = today.getFullYear();
                let mm = today.getMonth() + 1; // Months start at 0!
                let dd = today.getDate();
        
                if (dd < 10) dd = '0' + dd;
                if (mm < 10) mm = '0' + mm;
        
                const formattedToday = dd + '/' + mm + '/' + yyyy;
        
                let time = new Date(Date.now()).toLocaleString().split(',')[1];
    
                var datetime = formattedToday + time;
        
                let total_harga = 0;
                items.map(item => {
                    const postDetailListRef = ref(db, 'Sales_detail');
                    const newDetailPostRef = push(postDetailListRef);
        
                    set(newDetailPostRef, {
                        item_id : item.id,
                        name : item.name,
                        price : item.price,
                        qty : item.qty,
                        sale_id: newPostRef.key
                    })
        
                    total_harga += parseFloat(item.price) * parseFloat(item.qty)
    
                    let new_stok = parseFloat(item.stok) - parseFloat(item.qty)
    
                    update(ref(db, 'Product/'+item.id), {
                        stok: '' + new_stok
                    })
                })
        
                set(newPostRef, {
                    tanggal: datetime,
                    total: total_harga
                })
                .then(() => {
                    Alert.alert(
                        '',
                        'berhasil',
                        [
                            {
                                text: 'OK',
                                onPress: () => navigation.navigate('Sale')
                            }
                        ]
                    )
                })
                .catch((error) => {
                    Alert.alert(
                        '',
                        'gagal'
                    )
                });
            }
            if (type == 'edit') {
                const dbref = ref(db);

                var saleDetailPast = [];

                var childNode = query(ref(db, 'Sales_detail/'), orderByChild('sale_id'), equalTo(sale.id));

                await get(childNode).then((snapshot) => {
                    if(snapshot.exists()) {
                        let data = JSON.stringify(snapshot);
                        data = JSON.parse(data);

                        for (var prop in data) {
                            if (data.hasOwnProperty(prop)) {
                                var innerObj = {item_id: data[prop].item_id, qty_change: parseFloat(data[prop].qty)};
                                
                                saleDetailPast.push(innerObj);

                                remove(ref(db, 'Sales_detail/'+prop));
                            }
                        }
                    }
                })

                var datetime = sale.tanggal;
        
                let total_harga = 0;
                items.map(item => {
                    const postDetailListRef = ref(db, 'Sales_detail');
                    const newDetailPostRef = push(postDetailListRef);
        
                    set(newDetailPostRef, {
                        item_id : item.id,
                        name : item.name,
                        price : item.price,
                        qty : item.qty,
                        sale_id: sale.id
                    })
        
                    total_harga += parseFloat(item.price) * parseFloat(item.qty)

                    let sama = false;

                    saleDetailPast.map(sDetail => {
                        if (item.id == sDetail.item_id) {
                            sama = true;
                            sDetail.qty_change -= parseFloat(item.qty);
                        }
                    })

                    if (!sama) {
                        saleDetailPast.push({
                            item_id: item.id,
                            qty_change: parseFloat(item.qty) * -1
                        })
                    }
                })

                saleDetailPast.map(item => {
                    if (item.qty_change != 0) {
                        get(child(dbref, 'Product/' + item.item_id)).then((snapshot) => {
                            if(snapshot.exists()) {    
                                let data = JSON.stringify(snapshot);
                                data = JSON.parse(data);

                                let new_stok = parseFloat(data.stok) + item.qty_change;
        
                                update(ref(db, 'Product/'+item.item_id), {
                                    stok: '' + new_stok
                                })
                            }
                        });
                    }
                })
        
                update(ref(db, 'Sales/'+sale.id), {
                    tanggal: datetime,
                    total: total_harga
                })
                .then(() => {
                    Alert.alert(
                        '',
                        'berhasil',
                        [
                            {
                                text: 'OK',
                                onPress: () => navigation.navigate('Sale')
                            }
                        ]
                    )
                })
                .catch((error) => {
                    Alert.alert(
                        '',
                        'gagal'
                    )
                });
            }
        }
    }

    const getProduct = (pengenal) => {
        const db = startFirebase();
        const dbref = ref(db);

        get(child(dbref, 'Product/')).then((snapshot) => {
            if(snapshot.exists()) {
                let data = JSON.stringify(snapshot);
                data = JSON.parse(data);

                var arr = [];

                for (var prop in data) {
                    if (data.hasOwnProperty(prop)) {
                        var innerObj = {};
                        innerObj[prop] = data[prop];
                        arr.push(innerObj)
                    }
                }

                var arrfinal = [];

                arr.map(item => {
                    for (var prop in item) {
                        if (item.hasOwnProperty(prop)) {
                            item[prop].id = prop

                            arrfinal.push(item[prop])
                        }
                    }
                })

                arrfinal.map(item => {
                    let harga_formatted = new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR"
                      }).format(item.harga);

                    let harga = harga_formatted.split(',');

                    item.harga_display = harga[0];
                })

                setProducts(arrfinal);

                if (pengenal != '') {
                    arrfinal.map(item => {
                        pengenal.map(peng => {
                            if (item.id == peng.id) {
                                item.stok = peng.stok;
                            }
                        })
                    })
                }

                setpermanentBarang(arrfinal);
            } else {
                setProducts([]);
                setpermanentBarang([]);
            }
        });
    }

    useEffect(() => {
        if (isFocused) {
            if (type == 'add') {
                getProduct('');
            }
            setkeywordSearch('');
        }
    }, [isFocused])

    const [products, setProducts] = useState([]);

    const [items, setitems] = useState([]);
    const [jumlah, setJumlah] = useState("1");
    const [harga, setharga] = useState('');
    const [namaBarang, setnamaBarang] = useState('')
    const [idBarang, setidBarang] = useState('')
    const [stok, setstok] = useState('')

    const addItem = () => {
        if (parseFloat(jumlah) > 0) {
            let array = [...items];
    
            if (idBarang != '') {
                let sama = false;
    
                array.map(item => {
                    if (item.id == idBarang) {
                        sama = true;
                    }
                })
    
                if (sama) {
                    Alert.alert('Anda sudah menambahkan produk ini');
                } else {    
                    array.push({id: idBarang, name: namaBarang, qty: jumlah, price: harga, stok: stok});
                }
            } else {
                Alert.alert('Anda belum memilih produk');
            }
    
            setitems(array);
            setJumlah("1");
            setharga('');
            setnamaBarang('');
            setidBarang('');
            setstok('');
        } else {
            Alert.alert('Isi jumlah dengan benar');
        }
    }

    const [modalVisible, setModalVisible] = useState(false)

    const selectItemModal = () => {
        setModalVisible(true);
        setkeywordSearch('');
        handleSearch('');
    }

    const [keywordSearch, setkeywordSearch] = useState('')
    const [permanentBarang, setpermanentBarang] = useState([])

    const handleSearch = (keyword) => {
        setkeywordSearch(keyword);

        let products = [...permanentBarang];

        let new_products = [];

        if (products.length != 0) {
            products.map(item => {
                if (item.nama.includes(keyword)) {
                    new_products.push(item);
                }
            })
        }

        setProducts(new_products);
    }

    const handlePick = (item) => {
        if (item.stok > 0) {
            setModalVisible(false);
            setnamaBarang(item.nama);
            setharga(item.harga);
            setidBarang(item.id);
            setstok(item.stok);
        } else {
            Alert.alert('Stok dari produk ini tidak tersedia');
        }
    }

    const handleDelete = (item_index) => {
        let items_selected = [...items];

        let new_items = [];

        items_selected.map((item, index) => {
            if (!(index == item_index)) {
                new_items.push(item);
            }
        })

        setitems(new_items);
    }

    const editJumlah = (item_index, value) => {
        let items_selected = [...items];

        let new_items = [];

        items_selected.map((item, index) => {
            if (index == item_index) {
                if (parseFloat(value) > parseFloat(item.stok)) {
                    Alert.alert('Stok tidak mencukupi');
                    item.qty = item.stok;
                    new_items.push(item);
                } else {
                    item.qty = value;
                    new_items.push(item);
                }
            } else {
                new_items.push(item);
            }
        })

        setitems(new_items);
    }

    const setsJumlah = (value) => {
        if (parseFloat(value) > parseFloat(stok)) {
            Alert.alert('Stok tidak mencukupi');

            setJumlah(stok);
        } else {
            setJumlah(value);
        }
    }

    return (
        <View style={{alignItems: 'center', flex: 1, backgroundColor: '#cccccc'}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}>
                <View style={styless.centeredView}>
                    <View style={styless.modalView}>
                        <View style={{marginBottom: 5}}>
                            <View style={{justifyContent: 'center', flexDirection: 'row'}}>
                                <View style={{width: '50%', alignSelf: 'center'}}>
                                    <View style={{width: 50}}>
                                        <TouchableOpacity onPress={() => setModalVisible(false)} style={{backgroundColor: 'gray', borderRadius: 1000, alignSelf: 'center'}}><Text style={{color: 'white'}}><AntIcon name="closecircleo" size={35} /></Text></TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{width: '50%', alignSelf: 'center'}}>
                                    <TextInput value={keywordSearch} placeholder='Search product' style={styles.inputNormSearch} onChangeText={newText => handleSearch(newText)}></TextInput>
                                </View>
                            </View>
                        </View>
                        <ScrollView style={{width: '100%', height: 400}} contentInsetAdjustmentBehavior="automatic">
                            {
                                products &&
                                products.map((item, index) =>
                                    <View key={index} style={{backgroundColor: '#ccc', borderRadius: 5, flexDirection: 'row', width: '100%', height: 75, marginTop: 5, marginBottom: 5}}>
                                        <View style={{width: '35%', height: 60, alignSelf: 'center', borderRadius: 3, borderColor: 'white', overflow: 'hidden', marginRight: 10}}>
                                            {
                                                item.image_URL ?
                                                    <Image source={{uri: item.image_URL}} style={{flex: 1, width: undefined, height: undefined}} resizeMode="center"></Image>
                                                :
                                                    <Text style={styles.textboldcenter}><Feather name="image" size={60} /></Text>
                                            }
                                        </View>
                                        <View style={{width: '35%', height: 75, alignSelf: 'flex-start', justifyContent: 'center'}}>
                                            <Text style={{color: 'black', fontFamily: 'Poppins-Regular'}}>{item.nama}</Text>
                                            <Text style={styles.textnocenter1}>Stok: {item.stok}</Text>
                                            <Text style={styles.textnocenter}>{item.harga_display}</Text>
                                        </View>
                                        <View style={{width: '30%', height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                            <TouchableOpacity onPress={() => handlePick(item)} style={{backgroundColor: 'green', borderRadius: 10, paddingVertical: 10, paddingHorizontal: 10, alignSelf: 'center'}}><Text style={{color: 'white', fontFamily: 'Poppins-Regular'}}>Pick</Text></TouchableOpacity>
                                        </View>
                                    </View>
                                )
                            }
                        </ScrollView>
                    </View>
                </View>
            </Modal>
            <View style={{width: '100%', padding: 10}}>
                <TouchableOpacity onPress={() => selectItemModal()} style={{borderTopLeftRadius: 2, borderTopRightRadius: 2, backgroundColor: 'white', paddingVertical: 9.4, paddingHorizontal: 13, borderColor: 'black', borderWidth: 1, flexDirection: 'row'}}><Text style={{fontFamily: 'Poppins-Regular', width: '95%', color: 'black'}}>Pilih produk</Text><Text style={{width: '5%', color: 'black'}}><IonIcon name="md-chevron-down" size={15} /></Text></TouchableOpacity>
                {
                    (namaBarang == '' && harga == '') ?
                        <View style={{width: '100%', flexDirection: 'row', borderColor: 'black', borderRightWidth: 1, borderLeftWidth: 1, borderBottomWidth: 1, borderBottomLeftRadius: 2, borderBottomRightRadius: 2, marginBottom: 5}}>
                            <View style={{width: '35%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center'}}>
                                <TextInput editable={false} placeholder='nama produk' style={styles.inputNorm} value={namaBarang}></TextInput>
                            </View>
                            <View style={{width: '25%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center'}}>
                                <TextInput editable={false} keyboardType='numeric' placeholder='harga' style={styles.inputNorm} value={harga}></TextInput>
                            </View>
                            <View style={{width: '15%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center'}}>
                                <TextInput editable={false} keyboardType='numeric' placeholder='stok' style={styles.inputNorm} value={stok}></TextInput>
                            </View>
                            <View style={{width: '15%'}}>
                                <TextInput editable={false} keyboardType='numeric' placeholder='jumlah' style={styles.inputNorm} value={jumlah} onChangeText={newText => setJumlah(newText)}></TextInput>
                            </View>
                            <View style={{width: '10%', height: 40, justifyContent: 'center'}}>
                                <TouchableOpacity style={{width: '100%', borderLeftWidth: 1, height: '100%', justifyContent: 'center', backgroundColor: 'green'}} onPress={addItem}><Text style={{alignSelf: 'center', color: 'white'}}><AntIcon name="plus" size={15}></AntIcon></Text></TouchableOpacity>
                            </View>
                        </View>
                    :
                        <View style={{width: '100%', flexDirection: 'row', borderColor: 'black', borderRightWidth: 1, borderLeftWidth: 1, borderBottomWidth: 1, borderBottomLeftRadius: 2, borderBottomRightRadius: 2, marginBottom: 5}}>
                            <View style={{width: '35%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center'}}>
                                <TextInput editable={false} placeholder='nama produk' style={styles.inputSelected} value={namaBarang}></TextInput>
                            </View>
                            <View style={{width: '25%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center'}}>
                                <TextInput editable={false} keyboardType='numeric' placeholder='harga' style={styles.inputSelected} value={'Rp.' + harga}></TextInput>
                            </View>
                            <View style={{width: '15%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center'}}>
                                <TextInput editable={false} keyboardType='numeric' placeholder='stok' style={styles.inputSelected} value={stok}></TextInput>
                            </View>
                            <View style={{width: '15%'}}>
                                <TextInput keyboardType='numeric' placeholder='jumlah' style={styles.inputNorm} value={jumlah} onChangeText={newText => setsJumlah(newText)}></TextInput>
                            </View>
                            <View style={{width: '10%', height: 40, justifyContent: 'center'}}>
                                <TouchableOpacity style={{width: '100%', borderLeftWidth: 1, height: '100%', justifyContent: 'center', backgroundColor: 'green'}} onPress={addItem}><Text style={{alignSelf: 'center', color: 'white'}}><AntIcon name="plus" size={15}></AntIcon></Text></TouchableOpacity>
                            </View>
                        </View>
                }
                <Text style={{marginTop: 10, marginBottom: 10, color: 'black', fontFamily: 'Poppins-Regular'}}>Daftar barang</Text>
                <ScrollView style={{height: 200}} contentInsetAdjustmentBehavior="automatic">
                    <View style={{width: '100%', flexDirection: 'row', borderWidth: 1, bordercolor: 'black', backgroundColor: 'green'}}>
                        <View style={{width: '30%', borderRightWidth: 1, height: 40, justifyContent: 'center', padding: 5}}><Text style={{color: 'white', fontFamily: 'Poppins-Regular', fontSize: 12}}>Nama produk</Text></View>
                        <View style={{width: '28%', borderRightWidth: 1, height: 40, justifyContent: 'center', padding: 5}}><Text style={{color: 'white', fontFamily: 'Poppins-Regular', fontSize: 12}}>Harga satuan</Text></View>
                        <View style={{width: '15%', borderRightWidth: 1, height: 40, justifyContent: 'center', padding: 5}}><Text style={{color: 'white', fontFamily: 'Poppins-Regular', fontSize: 12}}>Stok</Text></View>
                        <View style={{width: '17%', height: 40, justifyContent: 'center', padding: 5}}><Text style={{color: 'white', fontFamily: 'Poppins-Regular', fontSize: 12}}>Jumlah</Text></View>
                        <View style={{width: '10%', height: 40, justifyContent: 'center'}}>
                            <View style={{width: '100%', borderLeftWidth: 1, height: '100%', justifyContent: 'center', backgroundColor: 'green'}}>
                                <Text style={{alignSelf: 'center', color: 'white'}}><Feather name="tool" size={15}></Feather></Text>
                            </View>
                        </View>
                    </View>
                    {
                        items &&
                        items.length > 0 ?
                        items.map((item, index) => 
                            <View key={index} style={{width: '100%', flexDirection: 'row', borderColor: 'black', borderRightWidth: 1, borderLeftWidth: 1, borderBottomWidth: 1}}>
                                <View style={{width: '30%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center', padding: 5, backgroundColor: 'white'}}><Text style={{fontFamily: 'Poppins-Regular'}}>{item.name}</Text></View>
                                <View style={{width: '28%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center', padding: 5, backgroundColor: 'white'}}><Text style={{fontFamily: 'Poppins-Regular'}}>Rp.{item.price}</Text></View>
                                <View style={{width: '15%', borderRightWidth: 1, borderRightColor: 'black', height: 40, justifyContent: 'center', padding: 5, backgroundColor: 'white'}}><Text style={{fontFamily: 'Poppins-Regular'}}>{item.stok}</Text></View>
                                <View style={{width: '17%'}}>
                                    <TextInput keyboardType='numeric' placeholder='jumlah' style={styles.inputNorm1} value={item.qty} onChangeText={newText => editJumlah(index, newText)}></TextInput>
                                </View>
                                <View style={{width: '10%', height: 40, justifyContent: 'center'}}>
                                    <TouchableOpacity style={{width: '100%', borderLeftWidth: 1, height: '100%', justifyContent: 'center', backgroundColor: 'red'}} onPress={() => handleDelete(index)}><Text style={{alignSelf: 'center', color: 'white'}}><Feather name="trash-2" size={15}></Feather></Text></TouchableOpacity>
                                </View>
                            </View>
                        ) :
                        <View style={{width: '100%', flexDirection: 'row', borderColor: 'black', borderRightWidth: 1, borderLeftWidth: 1, borderBottomWidth: 1}}>
                            <View style={{width: '100%', height: 40, justifyContent: 'center', padding: 5}}><Text style={{fontFamily: 'Poppins-Regular'}}>Belum ada produk yang ditambahkan</Text></View>
                        </View>
                    }
                </ScrollView>
                {
                    type &&
                    type == 'add' ?
                        <Button onPress={create} title='tambah penjualan'></Button>
                    :
                        <Button onPress={create} title='edit penjualan'></Button>
                }
            </View>
        </View>
    );
}

export default SaleAdd;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular', 
        color: '#808080'
    },
    textnocenter: {
        fontSize: 13, 
        fontFamily: 'Poppins-Regular',
        color: '#42baf8'
    },
    textnocenter1: {
        fontSize: 13, 
        fontFamily: 'Poppins-Regular'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    inputNorm: {
        backgroundColor: 'white',
        height: 40,
        fontFamily: 'Poppins-Regular',
        fontSize: 12
    },
    inputNormSearch: {
        backgroundColor: 'white',
        borderWidth: 1,
        height: 40
    },
    inputSelected: {
        backgroundColor: '#c2f0c2',
        height: 40,
        fontFamily: 'Poppins-Regular',
        fontSize: 12
    },
    inputNorm1: {
        backgroundColor: 'white',
        height: 40,
        fontFamily: 'Poppins-Regular',
        fontSize: 12
    }
});

const styless = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        width: '80%',
        padding: 15,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
    },
    buttonOpen: {
        backgroundColor: '#F194FF',
    },
    buttonClose: {
        backgroundColor: '#2196F3',
    },
    textStyle: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
});