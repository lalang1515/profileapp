import React from 'react';
import { Button, ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'

const Skills_detail = ( {route, navigation} ) => { 
    const { item } = route.params;

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic">
            <View style={{alignItems: 'center', flex: 1, backgroundColor: '#808080'}}>
                <View style={{alignItems: 'center', justifyContent: 'center', backgroundColor: item.color, width: 360, height: 640, marginTop: 20, marginBottom: 20, borderRadius: 10}}>
                    {
                    item.icon_name == 'IonIcon' ?
                        <IonIcon name={item.icon} size={300} />
                    : item.icon_name == 'MaterialIcon' ?
                        <MaterialIcon name={item.icon} size={300} />
                    : item.icon_name == 'Feather' ?
                        <Feather name={item.icon} size={300} />
                    :
                        <Icon name={item.icon} size={300} />
                    }
                </View>
            </View>
        </ScrollView>
    );
};

export default Skills_detail;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#808080'
    },
    textnocenter: {
        fontSize: 18, 
        fontFamily: 'Poppins-Regular',
        color: '#808080'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
});