import { initializeApp } from "firebase/app"
// import { getFirestore } from "firebase/firestore"
import { getDatabase } from "firebase/database"

function startFirebase() {
    const firebaseConfig = {
        apiKey: "AIzaSyDw1xcA_D5PWZbxqBqpyQZqjhWdhxmy1Hs",
        authDomain: "jolali-app.firebaseapp.com",
        databaseURL: "https://jolali-app-default-rtdb.asia-southeast1.firebasedatabase.app",
        projectId: "jolali-app",
        storageBucket: "jolali-app.appspot.com",
        messagingSenderId: "688740675691",
        appId: "1:688740675691:web:a22ab57f8c9a295c0bc12e"
    };

    const app = initializeApp(firebaseConfig);
    return getDatabase(app);
}

export default startFirebase;