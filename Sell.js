import React from 'react';
import { Button, ScrollView, Image, StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import { useEffect, useState } from 'react';
import startFirebase from './config/FIREBASE';
import { ref, push, set, get, update, remove, child, query, orderByChild, equalTo } from 'firebase/database';
import { useIsFocused } from '@react-navigation/native';
import { deleteObject, ref as sRef, getStorage } from 'firebase/storage';

const Sell = ( {navigation} ) => { 
    const isFocused = useIsFocused()
    const [barang, setbarang] = useState([])
    const [loading, setloading] = useState(true)

    const getSales = () => {
        setloading(true);

        const db = startFirebase();
        const dbref = ref(db);

        get(child(dbref, 'Sales/')).then((snapshot) => {
            if(snapshot.exists()) {
                let data = JSON.stringify(snapshot);
                data = JSON.parse(data);

                var arr = [];

                for (var prop in data) {
                    if (data.hasOwnProperty(prop)) {
                        var innerObj = {};
                        innerObj[prop] = data[prop];
                        arr.push(innerObj)
                    }
                }

                var arrfinal = [];

                arr.map(item => {
                    for (var prop in item) {
                        if (item.hasOwnProperty(prop)) {
                            item[prop].id = prop

                            arrfinal.push(item[prop])
                        }
                    }
                })

                arrfinal.map(item => {
                    let total_formatted = new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR"
                    }).format(item.total);

                    let total_display = total_formatted.split(',');

                    item.total_display = total_display[0];

                    let date = item.tanggal.substring(0, 10);
                    let time = item.tanggal.substring(11);

                    let split_date = date.split('/');

                    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

                    item.tanggal_aj = split_date[0] + ' ' + monthNames[split_date[1] - 1] + ' ' + split_date[2];
                    item.time = time;
                })

                setbarang(arrfinal);

                setloading(false);
            } else {
                setbarang([]);

                setloading(false);
            }
        });
    }

    const deleteSales = async(sale) => {
        const db = startFirebase();
        const dbref = ref(db);

        remove(ref(db, 'Sales/'+sale.id));

        var childNode = query(ref(db, 'Sales_detail/'), orderByChild('sale_id'), equalTo(sale.id));

        await get(childNode).then((snapshot) => {
            if(snapshot.exists()) {
                let data = JSON.stringify(snapshot);
                data = JSON.parse(data);

                console.log(data);

                var arr = [];

                for (var prop in data) {
                    if (data.hasOwnProperty(prop)) {
                        var innerObj = {id: prop, item_id: data[prop].item_id, qty: data[prop].qty};
                        arr.push(innerObj)
                    }
                }

                if (arr.length > 0) {
                    let banyakBarang = arr.length;
                    let banyakBarangTerhapus = 0;

                    arr.map(item => {
                        remove(ref(db, 'Sales_detail/'+item.id));

                        get(child(dbref, 'Product/' + item.item_id)).then((snapshot) => {
                            if(snapshot.exists()) {
                                let data = JSON.stringify(snapshot);
                                data = JSON.parse(data);

                                let new_stok = parseFloat(data.stok) + parseFloat(item.qty)

                                update(ref(db, 'Product/'+item.item_id), {
                                    stok: '' + new_stok
                                })
                            }
                        });

                        banyakBarangTerhapus++;
                    })

                    if (banyakBarang == banyakBarangTerhapus) {
                        Alert.alert(
                            '',
                            'berhasil',
                            [
                                {
                                    text: 'OK',
                                    onPress: () => getSales()
                                }
                            ]
                        )
                    } else {
                        Alert.alert(
                            '',
                            'gagal'
                        )
                    }
                }
            }
        })
    }

    const deleteButton = (item) =>
    Alert.alert(
      "Delete Alert",
      "Are you sure you want to delete this product?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { text: "Yes", onPress: () => deleteSales(item) }
      ]
    );

    useEffect(() => {
        if (isFocused) {
            getSales();
        }
    }, [isFocused])

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic">
            <View style={{alignItems: 'center', backgroundColor: 'white'}}>
                <View style={{width: 335, marginTop: 15, marginBottom: 15, borderRadius: 10, alignItems: 'center', backgroundColor: '#cccccc'}}>
                    <View style={{borderRadius: 3, flexDirection: 'row', width: 305, marginTop: 15, marginBottom: 10}}>
                        <View style={{width: 305, justifyContent: 'center'}}>
                            <View style={{width: 50, alignSelf: 'flex-start'}}>
                                <Button onPress={() => navigation.navigate('Add Sale', {type: 'add'})} title='add'></Button>
                            </View>
                        </View>
                    </View>
                    {
                        loading &&
                        loading ?
                            <View style={{paddingLeft: 7, backgroundColor: 'white', borderRadius: 5, flexDirection: 'row', width: 305, height: 75, marginTop: 5, marginBottom: 10}}>
                                <View style={{width: 290, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                    <Text style={{fontFamily: 'Poppins-Regular', alignSelf: 'center'}}>Loading...</Text>
                                </View>
                            </View>
                        :
                        barang &&
                        barang.length > 0 ?
                        barang.map((item, index) => 
                            <View key={index} style={{paddingLeft: 7, backgroundColor: 'white', borderRadius: 5, flexDirection: 'row', width: 305, height: 75, marginTop: 5, marginBottom: 10}}>
                                <View style={{marginLeft: 3, width: 150, height: 75, alignSelf: 'flex-start', justifyContent: 'center'}}>
                                    <Text style={{fontFamily: 'Poppins-Regular'}}>{item.tanggal}</Text>
                                    <Text style={styles.textnocenter}>{item.total_display}</Text>
                                </View>
                                <View style={{width: 70, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Sale Detail', {sale: item})} style={{backgroundColor: 'blue', borderRadius: 10, paddingVertical: 10, paddingHorizontal: 13, alignSelf: 'center'}}><Text style={{color: 'white'}}><IonIcon name="eye" size={30} /></Text></TouchableOpacity>
                                </View>
                                <View style={{width: 70, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => deleteButton(item)} style={{backgroundColor: 'red', borderRadius: 10, paddingVertical: 10, paddingHorizontal: 15, alignSelf: 'center'}}><Text style={{color: 'white'}}><Icon name="trash-o" size={30} /></Text></TouchableOpacity>
                                </View>
                            </View>
                        )
                        :
                        <View style={{paddingLeft: 7, backgroundColor: 'white', borderRadius: 5, flexDirection: 'row', width: 305, height: 75, marginTop: 5, marginBottom: 10}}>
                            <View style={{width: 290, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                <Text style={{fontFamily: 'Poppins-Regular', alignSelf: 'center'}}>Tidak ada penjualan</Text>
                            </View>
                        </View>
                    }
                </View>
            </View>
        </ScrollView>
    );
}

export default Sell;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular', 
        color: '#808080'
    },
    textnocenter: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular',
        color: '#42baf8'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
});