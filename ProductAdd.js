import React, { useCallback, useEffect, useState } from 'react';
import { Button, Modal, ScrollView, Image, StyleSheet, Text, View, TextInput, Alert, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import startFirebase from './config/FIREBASE';
import { ref, push, set, get, update, remove, child } from 'firebase/database';
import DocumentPicker from 'react-native-document-picker';
import { getStorage, ref as sRef, uploadBytes, getDownloadURL, deleteObject } from 'firebase/storage'

const ProductAdd = ( {route, navigation} ) => { 
    const { type, product } = route.params;

    const [nama, setnama] = useState('');
    const [harga, setharga] = useState('');
    const [stok, setstok] = useState('0');

    useEffect(() => {
      if (type == 'edit') {
        setnama(product.nama)
        setharga(product.harga)
        setstok(product.stok)
      }
    }, [])

    if (type == 'edit') {
        useEffect(() => {
            setstok(product.stok)
        }, [product.stok])
    }
    
    const [fileResponse, setFileResponse] = useState([]);

    const handleDocumentSelection = useCallback(async () => {
        try {
        const response = await DocumentPicker.pick({
            presentationStyle: 'fullScreen',
        });
        setFileResponse(response);
        console.log(response);
        } catch (err) {
        console.warn(err);
        }
    }, []);

    const [modalVisible, setModalVisible] = useState(false)

    const create = async () => {
        if (!nama || !harga) {
            Alert.alert(
                'Data belum lengkap!'
            )
            return false;
        }

        if (fileResponse.length == 0) {
            Alert.alert(
                'Wajib menyertai gambar!'
            )
            return false;
        }

        setModalVisible(true)

        const db = startFirebase();

        const storage = getStorage();

        var currentdate = new Date(); 
        var datetime = currentdate.getDate() + ""
            + (currentdate.getMonth()+1)  + "" 
            + currentdate.getFullYear() + ""  
            + currentdate.getHours() + ""  
            + currentdate.getMinutes() + "" 
            + currentdate.getSeconds();

        const storageRef = sRef(storage, "Products_img/" + fileResponse[0].name + datetime);

        const image = await fetch(fileResponse[0].uri);
        const bytes = await image.blob();

        await uploadBytes(storageRef, bytes).then((snapshot) => {
            getDownloadURL(snapshot.ref).then(function(downloadURL) {
                const postListRef = ref(db, 'Product');

                const newPostRef = push(postListRef);

                set(newPostRef, {
                    nama: nama,
                    harga: harga,
                    image_URL: downloadURL,
                    image_path: snapshot.metadata.fullPath,
                    stok: stok
                })
                .then(() => {
                    setModalVisible(false)
                    Alert.alert(
                        '',
                        'berhasil',
                        [
                            {
                                text: 'OK',
                                onPress: () => navigation.navigate('Product')
                            }
                        ]
                    )
                })
                .catch((error) => {
                    setModalVisible(false);
                    Alert.alert(
                        '',
                        'gagal'
                    )
                });
            })
        });
    }

    const update_product = async (item) => {
        const db = startFirebase();

        if (!nama || !harga) {
            Alert.alert(
                'Data belum lengkap!'
            )
            return false;
        }

        setModalVisible(true);

        if (fileResponse.length <= 0) {
            update(ref(db, 'Product/'+item.id), {
                nama: nama,
                harga: harga
            })
            .then(() => {
                setModalVisible(false);
                Alert.alert(
                    '',
                    'berhasil',
                    [
                        {
                            text: 'OK',
                            onPress: () => navigation.navigate('Product')
                        }
                    ]
                )
            })
            .catch((error) => {
                setModalVisible(false);
                Alert.alert(
                    '',
                    'gagal'
                )
            });
        } else {
            const storage = getStorage();

            var currentdate = new Date(); 
            var datetime = currentdate.getDate() + ""
                + (currentdate.getMonth()+1)  + "" 
                + currentdate.getFullYear() + ""  
                + currentdate.getHours() + ""  
                + currentdate.getMinutes() + "" 
                + currentdate.getSeconds();
            
            const storageRef = sRef(storage, "Products_img/" + fileResponse[0].name + datetime);

            const image = await fetch(fileResponse[0].uri);
            const bytes = await image.blob();

            await uploadBytes(storageRef, bytes).then((snapshot) => {
                getDownloadURL(snapshot.ref).then(function(downloadURL) {
                    update(ref(db, 'Product/'+item.id), {
                        nama: nama,
                        harga: harga,
                        image_URL: downloadURL,
                        image_path: snapshot.metadata.fullPath
                    })
                    .then(() => {
                        setModalVisible(false);
                        Alert.alert(
                            '',
                            'berhasil',
                            [
                                {
                                    text: 'OK',
                                    onPress: () => navigation.navigate('Product')
                                }
                            ]
                        )
                    })
                    .catch((error) => {
                        setModalVisible(false);
                        Alert.alert(
                            '',
                            'gagal'
                        )
                    });
                })
            });

            deleteObject(sRef(storage, product.image_path));
        }
    }

    const handleStok = (value) => {
        if (parseFloat(value) < 0) {
            setstok('0');
        } else {
            value = JSON.stringify(value);

            let sttt = value.split('-');

            let stt = '';

            sttt.map(item => {
                if (item != '') {
                    stt += item;
                }
            });

            let panjang = stt.length;

            stt = stt.substring(1, panjang - 1);

            stt = parseFloat(stt);

            if (isNaN(stt)) {
                stt = 0;
            }

            setstok('' + stt);
        }
    }

    return (
        <View style={{alignItems: 'center', flex: 1, backgroundColor: '#cccccc'}}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                Alert.alert('Modal has been closed.');
                setModalVisible(!modalVisible);
                }}>
                <View style={styless.centeredView}>
                <View style={styless.modalView}>
                    <Text style={styless.modalText}>Loading...</Text>
                </View>
                </View>
            </Modal>
            <View style={{width: 320, paddingTop: 10, paddingBottom: 10}}>
                <Text style={{fontFamily: 'Poppins-Regular'}}>Nama Produk</Text>
                <TextInput placeholder='nama produk' style={styles.inputNorm} defaultValue={nama} onChangeText={newText => setnama(newText)}></TextInput>
                <Text style={{fontFamily: 'Poppins-Regular'}}>Harga</Text>
                <TextInput keyboardType='numeric' placeholder='harga' style={styles.inputNorm} defaultValue={harga} onChangeText={newText => setharga(newText)}></TextInput>
                {
                    type &&
                    type == 'add' ?
                        <View>
                        <Text style={{fontFamily: 'Poppins-Regular'}}>Stok awal</Text>
                        <TextInput keyboardType='numeric' placeholder='stok awal' style={styles.inputNorm} value={stok} onChangeText={newText => handleStok(newText)}></TextInput>
                        </View>
                    :
                        <View>
                            <Text style={{fontFamily: 'Poppins-Regular'}}>Stok</Text>
                            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                <View style={{width: '82%'}}>
                                    {
                                        stok &&
                                        stok ?
                                            <TextInput editable={false} keyboardType='numeric' placeholder='Stok terkini' style={styles.inputNorm} value={stok}></TextInput>
                                        :
                                            <TextInput editable={false} keyboardType='numeric' placeholder='Stok terkini' style={styles.inputNorm}></TextInput>
                                    }
                                </View>
                                <View style={{width: '18%'}}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Stok Opname', {productSelected: product})} style={{borderRadius: 2, backgroundColor: 'yellow', paddingVertical: 9.4, paddingHorizontal: 13, borderColor: 'black', borderWidth: 1, alignItems: 'center'}}><Text style={{color: 'black'}}><Icon name="edit" size={30} /></Text></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                }
                <Text style={{fontFamily: 'Poppins-Regular'}}>Pilih Gambar</Text>
                <TouchableOpacity onPress={handleDocumentSelection} style={{borderWidth: 1, marginBottom: 10, borderRadius: 2, justifyContent: 'center', backgroundColor: 'white', width: 70, height: 70}}>
                    {
                        type &&
                        type == 'add' ?
                            fileResponse[0] ?
                            <Image style={{flex: 1, width: undefined, height: undefined}} resizeMode="center" source={{uri: fileResponse[0].uri}}></Image>
                            :
                            <Text style={styles.textboldcenter}><Feather name="image" size={60} /></Text>
                        :
                            fileResponse[0] ?
                            <Image style={{flex: 1, width: undefined, height: undefined}} resizeMode="center" source={{uri: fileResponse[0].uri}}></Image>
                            :
                                product.image_URL ?
                                <Image style={{flex: 1, width: undefined, height: undefined}} resizeMode="center" source={{uri: product.image_URL}}></Image>
                                :
                                <Text style={styles.textboldcenter}><Feather name="image" size={60} /></Text>
                    }
               </TouchableOpacity>
                {
                    type &&
                    type == 'add' ?
                        <Button onPress={create} title='add'></Button>
                    :
                        <Button onPress={() => {update_product(product)}} title='edit'></Button>
                }
            </View>
        </View>
    );
}

export default ProductAdd;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular', 
        color: '#808080',
        alignSelf: 'center'
    },
    textnocenter: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular',
        color: '#42baf8'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    inputNorm: {
        borderWidth: 1,
        marginBottom: 10,
        borderRadius: 2,
        backgroundColor: 'white',
        fontFamily: 'Poppins-Regular'
    }
});

const styless = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 22,
    },
    modalView: {
      margin: 20,
      backgroundColor: 'white',
      borderRadius: 20,
      padding: 35,
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
    },
    buttonOpen: {
      backgroundColor: '#F194FF',
    },
    buttonClose: {
      backgroundColor: '#2196F3',
    },
    textStyle: {
      color: 'white',
      fontWeight: 'bold',
      textAlign: 'center',
    },
    modalText: {
      marginBottom: 15,
      textAlign: 'center',
    },
  });