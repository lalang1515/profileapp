import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'

const data = [
  {name: 'HTML', icon_name: 'Icon', icon: 'html5', text: 'Noob pol gabisa apa2', color: '#ff6600', type: 'left'},
  {name: 'CSS', icon_name: 'Icon', icon: 'css3', text: 'Kaga bisa desain', color: '#0066ff', type: 'right'},
  {name: 'Javascript', icon_name: 'IonIcon', icon: 'ios-logo-javascript', text: 'Not bad', color: '#ffff00', type: 'left'},
  {name: 'PHP', icon_name: 'MaterialIcon', icon: 'language-php', text: 'Full mumet', color: '#9999ff', type: 'right'},
  {name: 'Tailwind', icon_name: 'MaterialIcon', icon: 'tailwind', text: 'Half decent', color: '#66ccff', type: 'left'},
  {name: 'GIT', icon_name: 'Feather', icon: 'git-branch', text: 'Agak dong', color: '#ff3333', type: 'right'}
]

const Skills = ({ navigation }) => { 
  return (
    <ScrollView contentInsetAdjustmentBehavior="automatic">

         <View style={{alignItems: "center", backgroundColor: '#262626'}}>
           <View style={{width: 300, height: 50, marginTop: 20, borderRadius: 3}}>
             <Text style={{alignSelf: "center", color: 'white', fontSize: 24, fontFamily: 'monospace', fontWeight: 'bold'}}>S K I L L S</Text>
           </View>
         </View>
         <View style={{alignItems: 'center', flex: 1, backgroundColor: '#808080', paddingBottom: 20}}>

          {
            data.map((item, index) =>
              item.type == 'left' ?
                <View key={index} style={{flexDirection: 'row', marginTop: 20}}>
                  <View style={{backgroundColor: item.color, width: 105, height: 80,  marginRight: 10, borderRadius: 10}}>
                    <View style={{width: 110}}>
                      <View style={{height: 65, marginTop: 9}}>
                        <Text onPress={() => navigation.navigate('Skills_detail', {item: item})} style={styles.textboldcenter}>
                          {
                            item.icon_name == 'IonIcon' ?
                              <IonIcon name={item.icon} size={60} />
                            : item.icon_name == 'MaterialIcon' ?
                              <MaterialIcon name={item.icon} size={60} />
                            : item.icon_name == 'Feather' ?
                              <Feather name={item.icon} size={60} />
                            :
                              <Icon name={item.icon} size={60} />
                          }
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={{backgroundColor: 'white', width: 245, height: 80, paddingLeft: 10,  borderRadius: 10}}>
                    <View style={{width: 250}}>
                      <View style={{height: 65, marginTop: 9}}>
                        <Text style={styles.textbold}>{item.name}</Text>
                        <Text style={styles.textnocenter}>{item.text}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              :
              <View key={index} style={{flexDirection: 'row', marginTop: 20}}>
                <View style={{backgroundColor: 'white', width: 245, height: 80, paddingLeft: 10,  borderRadius: 10}}>
                  <View style={{width: 250}}>
                    <View style={{height: 65, marginTop: 9}}>
                      <Text style={styles.textbold}>{item.name}</Text>
                      <Text style={styles.textnocenter}>{item.text}</Text>
                    </View>
                  </View>
                </View>
                <View style={{backgroundColor: item.color, width: 105, height: 80,  marginLeft: 10, borderRadius: 10}}>
                  <View style={{width: 110}}>
                    <View style={{height: 65, marginTop: 9}}>
                      <Text onPress={() => navigation.navigate('Skills_detail', {item: item})} style={styles.textboldcenter}>
                        {
                          item.icon_name == 'IonIcon' ?
                            <IonIcon name={item.icon} size={60} />
                          : item.icon_name == 'MaterialIcon' ?
                            <MaterialIcon name={item.icon} size={60} />
                          : item.icon_name == 'Feather' ?
                            <Feather name={item.icon} size={60} />
                          :
                            <Icon name={item.icon} size={60} />
                        }
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            )
          }

         </View>
       </ScrollView>
  );
};

export default Skills;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#808080'
    },
    textnocenter: {
        fontSize: 18, 
        fontFamily: 'Poppins-Regular',
        color: '#808080'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
});