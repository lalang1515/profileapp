import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Skills from './Skills';
import Home from './Profile';
import Icon from "react-native-vector-icons/FontAwesome";
import Skills_detail from './Skills_detail';

const Tab = createBottomTabNavigator();

const BottomTab = () => { 
return (
    <Tab.Navigator
        screenOptions={({route}) => ({
            tabBarIcon: ({focused, color, size}) => {
                let iconName;

                if (route.name === 'Skills') {
                    iconName = focused ? 'code' : 'code'
                } else if (route.name === 'Home') {
                    iconName = focused ? 'home' : 'home'
                }

                return <Icon name={iconName} size={size} color={color} />;
            }
        })}
    >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Skills" component={Skills} />
    </Tab.Navigator>
);
};

export default BottomTab;
