import { useNetInfo } from '@react-native-community/netinfo';
import React, { useEffect } from 'react';
import { ScrollView, Image, StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";

const Profile = ({ navigation }) => { 
  const netInfo = useNetInfo();

  const networkCheck = () => {
    if (netInfo.isInternetReachable) {
      Alert.alert("You are online!");
    } else {
      Alert.alert("You are offline!", "", 
      [
        {
          text: 'Try again',
          onPress: () => networkCheck()
        }
      ]);
    }
  }

  useEffect(() => {
    networkCheck();
  }, [netInfo])

  return (
    <ScrollView contentContainerStyle={{flex: 1, backgroundColor: '#cccccc'}} contentInsetAdjustmentBehavior="automatic">
         <View style={{alignItems: 'center'}}>
           <View style={{flexDirection: 'row', width: 300, marginTop: 20, marginBottom: 25}}>
            <View style={{width: 200, height: 75, alignSelf: 'flex-start', justifyContent: 'center'}}>
                <Text style={styles.textbold}>Hi, Ilalang</Text>
                <Text style={styles.textnocenter}>what do u want to do?</Text>
            </View>
            <View style={{width: 100}}>
              <View style={{width: 75, height: 75, alignSelf: 'flex-end', borderRadius: 1000, borderColor: 'white', overflow: 'hidden', backgroundColor: '#000000'}}>
                <Image source={require("./assets/lalang.jpg")} style={{flex: 1, width: undefined, height: undefined}} resizeMode="center"></Image>
              </View>
            </View>
           </View>
           <View style={{flexDirection: 'row', width: 340, marginTop: 5, marginBottom: 20}}>
              <View style={{width: 170}}>
               <TouchableOpacity onPress={() => navigation.navigate('Product')} style={{width: 145, height: 145, alignSelf: 'center', backgroundColor: 'white', borderRadius: 20, justifyContent: 'center'}}>
                 <Text style={styles.textboldcenter}><Icon name="coffee" size={50} /></Text>
                 <Text style={styles.text}>Product</Text>
               </TouchableOpacity>
             </View>
             <View style={{width: 170}}>
               <TouchableOpacity onPress={() => navigation.navigate('Sale')} style={{width: 145, height: 145, alignSelf: 'center', backgroundColor: 'white', borderRadius: 20, justifyContent: 'center'}}>
                 <Text style={styles.textboldcenter}><Icon name="shopping-bag" size={50} /></Text>
                 <Text style={styles.text}>Sale</Text>
               </TouchableOpacity>
             </View>
           </View>
           {/* <View style={{flexDirection: 'row', width: 340, marginTop: 5, marginBottom: 20}}>
             <View style={{width: 170}}>
               <TouchableOpacity onPress={() => navigation.navigate('Report')} style={{width: 145, height: 145, alignSelf: 'center', backgroundColor: 'white', borderRadius: 20, justifyContent: 'center'}}>
                 <Text style={styles.textboldcenter}><Icon name="area-chart" size={50} /></Text>
                 <Text style={styles.text}>Report</Text>
               </TouchableOpacity>
             </View>
             <View style={{width: 170}}>
               <TouchableOpacity onPress={() => navigation.navigate('Buy')} style={{width: 145, height: 145, alignSelf: 'center', backgroundColor: 'white', borderRadius: 20, justifyContent: 'center'}}>
                 <Text style={styles.textboldcenter}><Icon name="shopping-cart" size={50} /></Text>
                 <Text style={styles.text}>Buy</Text>
               </TouchableOpacity>
             </View>
           </View> */}
         </View>
       </ScrollView>
  );
};

export default Profile;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        marginTop: 5,
        fontSize: 18, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#808080'
    },
    textnocenter: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular',
        color: '#808080'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
});