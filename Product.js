import React from 'react';
import { TextInput, Button, Modal, ScrollView, Image, StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import { useEffect, useState } from 'react';
import startFirebase from './config/FIREBASE';
import { ref, push, set, get, update, remove, child } from 'firebase/database';
import { useIsFocused } from '@react-navigation/native';
import { deleteObject, ref as sRef, getStorage } from 'firebase/storage';

const Product = ( {navigation} ) => { 
    const isFocused = useIsFocused()
    const [barang, setbarang] = useState([])
    const [permanentBarang, setpermanentBarang] = useState([])
    const [loading, setloading] = useState(true)

    const getProduct = () => {
        const db = startFirebase();
        const dbref = ref(db);

        get(child(dbref, 'Product/')).then((snapshot) => {
            if(snapshot.exists()) {
                setloading(false);

                let data = JSON.stringify(snapshot);
                data = JSON.parse(data);

                var arr = [];

                for (var prop in data) {
                    if (data.hasOwnProperty(prop)) {
                        var innerObj = {};
                        innerObj[prop] = data[prop];
                        arr.push(innerObj)
                    }
                }

                var arrfinal = [];

                arr.map(item => {
                    for (var prop in item) {
                        if (item.hasOwnProperty(prop)) {
                            item[prop].id = prop

                            arrfinal.push(item[prop])
                        }
                    }
                })

                arrfinal.map(item => {
                    let harga_formatted = new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR"
                      }).format(item.harga);

                    let harga = harga_formatted.split(',');

                    item.harga_display = harga[0];
                })

                setbarang(arrfinal);
                setpermanentBarang(arrfinal);
            } else {
                setloading(false);

                setbarang([]);
                setpermanentBarang([]);
            }
        });
    }

    const [modalVisible, setModalVisible] = useState(false)

    const deleteProduct = (item) => {
        setModalVisible(true)
        const db = startFirebase();

        const storage = getStorage();

        if (item.image_path) {
            deleteObject(sRef(storage, item.image_path));
        }

        remove(ref(db, 'Product/'+item.id))
        .then(() => {
            setModalVisible(false)
            Alert.alert(
                '',
                'berhasil',
                [
                    {
                        text: 'OK',
                        onPress: () => {
                            getProduct();
                            setkeywordSearch('');
                        }
                    }
                ]
            )
        })
        .catch((error) => {
            Alert.alert(
                '',
                'gagal'
            )
        });
    }

    const deleteButton = (item) =>
    Alert.alert(
      "Delete Alert",
      "Are you sure you want to delete this product?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { text: "Yes", onPress: () => deleteProduct(item) }
      ]
    );

    useEffect(() => {
        if (isFocused) {
            getProduct();
            setkeywordSearch('');
        }
    }, [isFocused])

    const [keywordSearch, setkeywordSearch] = useState('');

    const handleSearch = (keyword) => {
        setkeywordSearch(keyword);

        let products = [...permanentBarang];

        let new_products = [];

        if (products.length != 0) {
            products.map(item => {
                if (item.nama.includes(keyword)) {
                    new_products.push(item);
                }
            })
        }

        setbarang(new_products);
    }

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic">
            <View style={{alignItems: 'center', backgroundColor: 'white'}}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                    setModalVisible(!modalVisible);
                    }}>
                    <View style={styless.centeredView}>
                    <View style={styless.modalView}>
                        <Text style={styless.modalText}>Loading...</Text>
                    </View>
                    </View>
                </Modal>
                <View style={{width: 335, marginTop: 15, marginBottom: 15, borderRadius: 10, alignItems: 'center', backgroundColor: '#cccccc'}}>
                    <View style={{borderRadius: 3, flexDirection: 'row', width: 305, marginTop: 15, marginBottom: 10}}>
                        <View style={{flex: 1, justifyContent: 'center', flexDirection: 'row'}}>
                            <View style={{width: '50%', alignSelf: 'center'}}>
                                <View style={{width: 50}}>
                                    <Button onPress={() => navigation.navigate('Add Product', {type: 'add'})} title='add'></Button>
                                </View>
                            </View>
                            <View style={{width: '50%', alignSelf: 'center'}}>
                                <TextInput value={keywordSearch} placeholder='Search product' style={styles.inputNorm} onChangeText={newText => handleSearch(newText)}></TextInput>
                            </View>
                        </View>
                    </View>
                    {
                        loading &&
                        loading ?
                            <View style={{paddingLeft: 7, backgroundColor: 'white', borderRadius: 5, flexDirection: 'row', width: 305, height: 75, marginTop: 5, marginBottom: 10}}>
                                <View style={{width: 290, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                    <Text style={{fontFamily: 'Poppins-Regular', alignSelf: 'center'}}>Loading...</Text>
                                </View>
                            </View>
                        :
                        barang &&
                        barang.length > 0 ?
                        barang.map((item, index) => 
                            <View key={index} style={{paddingLeft: 7, backgroundColor: 'white', borderRadius: 5, flexDirection: 'row', width: 305, height: 75, marginTop: 5, marginBottom: 10}}>
                                <View style={{width: 60, height: 60, alignSelf: 'center', borderRadius: 3, borderColor: 'white', overflow: 'hidden', marginRight: 10}}>
                                    {
                                        item.image_URL ?
                                            <Image source={{uri: item.image_URL}} style={{flex: 1, width: undefined, height: undefined}} resizeMode="center"></Image>
                                        :
                                            <Text style={styles.textboldcenter}><Feather name="image" size={60} /></Text>
                                    }
                                </View>
                                <View style={{width: 100, height: 75, alignSelf: 'flex-start', justifyContent: 'center'}}>
                                    <Text style={{fontFamily: 'Poppins-Regular'}}>{item.nama}</Text>
                                    <Text style={styles.textnocenter}>{item.harga_display}</Text>
                                </View>
                                <View style={{width: 60, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Edit Product', {type: 'edit', product: item})} style={{backgroundColor: 'green', borderRadius: 10, paddingVertical: 10, paddingHorizontal: 13, alignSelf: 'center'}}><Text style={{color: 'white'}}><Icon name="edit" size={30} /></Text></TouchableOpacity>
                                </View>
                                <View style={{width: 60, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => deleteButton(item)} style={{backgroundColor: 'red', borderRadius: 10, paddingVertical: 10, paddingHorizontal: 15, alignSelf: 'center'}}><Text style={{color: 'white'}}><Icon name="trash-o" size={30} /></Text></TouchableOpacity>
                                </View>
                            </View>
                        )
                        :
                        <View style={{paddingLeft: 7, backgroundColor: 'white', borderRadius: 5, flexDirection: 'row', width: 305, height: 75, marginTop: 5, marginBottom: 10}}>
                            <View style={{width: 290, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                <Text style={{fontFamily: 'Poppins-Regular', alignSelf: 'center'}}>Tidak ada produk</Text>
                            </View>
                        </View>
                    }
                </View>
            </View>
        </ScrollView>
    );
}

export default Product;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular', 
        color: '#808080'
    },
    textnocenter: {
        fontSize: 13, 
        fontFamily: 'Poppins-Regular',
        color: '#42baf8'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    inputNorm: {
        borderWidth: 1,
        borderRadius: 2,
        backgroundColor: 'white',
        height: 40
    }
});

const styless = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 22,
    },
    modalView: {
      margin: 20,
      backgroundColor: 'white',
      borderRadius: 20,
      padding: 35,
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    button: {
      borderRadius: 20,
      padding: 10,
      elevation: 2,
    },
    buttonOpen: {
      backgroundColor: '#F194FF',
    },
    buttonClose: {
      backgroundColor: '#2196F3',
    },
    textStyle: {
      color: 'white',
      fontWeight: 'bold',
      textAlign: 'center',
    },
    modalText: {
      marginBottom: 15,
      textAlign: 'center',
    },
  });