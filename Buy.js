import React from 'react';
import { Button, ScrollView, Image, StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import { useEffect, useState } from 'react';
import startFirebase from './config/FIREBASE';
import { ref, push, set, get, update, remove, child } from 'firebase/database';
import { useIsFocused } from '@react-navigation/native';
import { deleteObject, ref as sRef, getStorage } from 'firebase/storage';

const Buy = ( {navigation} ) => { 
    const isFocused = useIsFocused()
    const [barang, setbarang] = useState([])

    const getProduct = () => {
        const db = startFirebase();
        const dbref = ref(db);

        get(child(dbref, 'Product/')).then((snapshot) => {
            if(snapshot.exists()) {
                let data = JSON.stringify(snapshot);
                data = JSON.parse(data);

                var arr = [];

                for (var prop in data) {
                    if (data.hasOwnProperty(prop)) {
                        var innerObj = {};
                        innerObj[prop] = data[prop];
                        arr.push(innerObj)
                    }
                }

                var arrfinal = [];

                arr.map(item => {
                    for (var prop in item) {
                        if (item.hasOwnProperty(prop)) {
                            item[prop].id = prop

                            arrfinal.push(item[prop])
                        }
                    }
                })

                arr = JSON.stringify(arr);
                setbarang(arrfinal);

                // Alert.alert(
                //     '',
                //     JSON.stringify(arrfinal)
                // )
            } else {
                setbarang([]);

                // Alert.alert(
                //     '',
                //     'gaada'
                // )
            }
        });
    }

    const deleteProduct = (item) => {
        const db = startFirebase();

        const storage = getStorage();

        deleteObject(sRef(storage, item.image_path));

        remove(ref(db, 'Product/'+item.id))
        .then(() => {
            Alert.alert(
                '',
                'berhasil',
                [
                    {
                        text: 'OK',
                        onPress: () => getProduct()
                    }
                ]
            )
        })
        .catch((error) => {
            Alert.alert(
                '',
                'gagal'
            )
        });
    }

    const justAlert = () =>
    Alert.alert(
      "",
      "Product Deleted",
      [
        { text: "OK" }
      ]
    );

    const deleteButton = (item) =>
    Alert.alert(
      "Delete Alert",
      "Are you sure you want to delete this product?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        { text: "Yes", onPress: () => deleteProduct(item) }
      ]
    );

    useEffect(() => {
        if (isFocused) {
            getProduct();
        }
    }, [isFocused])

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic">
            <View style={{alignItems: 'center', backgroundColor: 'white'}}>
                <View style={{width: 335, marginTop: 15, marginBottom: 15, borderRadius: 10, alignItems: 'center', backgroundColor: '#cccccc'}}>
                    <View style={{borderRadius: 3, flexDirection: 'row', width: 305, marginTop: 15, marginBottom: 10}}>
                        <View style={{width: 305, justifyContent: 'center'}}>
                            <View style={{width: 50, alignSelf: 'flex-end'}}>
                                <Button onPress={() => navigation.navigate('ProductAdd', {type: 'add'})} title='add'></Button>
                            </View>
                        </View>
                    </View>
                    {
                        barang &&
                        barang.map((item, index) => 
                            <View key={index} style={{paddingLeft: 7, backgroundColor: 'white', borderRadius: 5, flexDirection: 'row', width: 305, height: 75, marginTop: 5, marginBottom: 10}}>
                                <View style={{width: 60, height: 60, alignSelf: 'center', borderRadius: 3, borderColor: 'white', overflow: 'hidden', backgroundColor: '#000000', marginRight: 10}}>
                                    <Image source={{uri: item.image_URL}} style={{flex: 1, width: undefined, height: undefined}} resizeMode="center"></Image>
                                </View>
                                <View style={{width: 90, height: 75, alignSelf: 'flex-start', justifyContent: 'center'}}>
                                    <Text style={{fontFamily: 'Poppins-Regular'}}>{item.nama}</Text>
                                    <Text style={styles.textnocenter}>Rp.{item.harga}</Text>
                                </View>
                                <View style={{width: 70, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => navigation.navigate('ProductAdd', {type: 'edit', product: item})} style={{backgroundColor: 'green', borderRadius: 10, paddingVertical: 10, paddingHorizontal: 13, alignSelf: 'center'}}><Text style={{color: 'white'}}><Icon name="edit" size={30} /></Text></TouchableOpacity>
                                </View>
                                <View style={{width: 70, height: 60, alignSelf: 'center', justifyContent: 'center'}}>
                                    <TouchableOpacity onPress={() => deleteButton(item)} style={{backgroundColor: 'red', borderRadius: 10, paddingVertical: 10, paddingHorizontal: 15, alignSelf: 'center'}}><Text style={{color: 'white'}}><Icon name="trash-o" size={30} /></Text></TouchableOpacity>
                                </View>
                            </View>
                        )
                    }
                </View>
            </View>
        </ScrollView>
    );
}

export default Buy;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular', 
        color: '#808080'
    },
    textnocenter: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular',
        color: '#42baf8'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
});