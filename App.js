import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import BottomTab from './BottomTab';
import Skills_detail from './Skills_detail';
import Product from './Product';
import ProductAdd from './ProductAdd';
import Profile from './Profile';
import Sell from './Sell';
import Buy from './Buy';
import Report from './Report';
import SaleAdd from './SaleAdd';
import Sale_detail from './Sale_detail';
import StokOpname from './StokOpname';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* <Stack.Screen
          name="Home"
          component={BottomTab}
          options={{ headerShown: false }}
        /> */}
        <Stack.Screen name="Home" component={Profile} />
        <Stack.Screen name="Skills_detail" component={Skills_detail} />
        <Stack.Screen name="Product" component={Product} />
        <Stack.Screen name="Add Product" component={ProductAdd} />
        <Stack.Screen name="Edit Product" component={ProductAdd} />
        <Stack.Screen name="Sale" component={Sell} />
        <Stack.Screen name="Add Sale" component={SaleAdd} />
        <Stack.Screen name="Edit Sale" component={SaleAdd} />
        <Stack.Screen name="Sale Detail" component={Sale_detail} />
        <Stack.Screen name="Buy" component={Buy} />
        <Stack.Screen name="Report" component={Report} />
        <Stack.Screen name="Stok Opname" component={StokOpname} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};