import React from 'react';
import { Button, ScrollView, Image, StyleSheet, Text, View, TouchableOpacity, Alert, PermissionsAndroid } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import { withSafeAreaInsets } from 'react-native-safe-area-context';
import { useEffect, useState } from 'react';
import startFirebase from './config/FIREBASE';
import { ref, push, set, get, update, remove, child } from 'firebase/database';
import { useIsFocused } from '@react-navigation/native';
import { deleteObject, ref as sRef, getStorage } from 'firebase/storage';
import Contacts from 'react-native-contacts';

const Report = ( {navigation} ) => { 
    const readKontak = () => {
        // PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        //     title: 'Contacts',
        //     message: 'This app would like to view your contacts.',
        //     buttonPositive: 'Please accept bare mortal',
        // })
        // .then((res) => {
        //     console.log('Permission: ', res);
        //     Contacts.getAll()
        //         .then((contacts) => {
        //             // work with contacts
        //             console.log(contacts);
        //         })
        //         .catch((e) => {
        //             console.log(e);
        //         });
        // })
        // .catch((error) => {
        //     console.error('Permission error: ', error);
        // });
        Contacts.getAll()
                .then((contacts) => {
                    // work with contacts
                    console.log(contacts);
                })
                .catch((e) => {
                    console.log(e);
                });
    }

    const addKontak = () => {
        // PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS, {
        //     title: 'Contacts',
        //     message: 'This app would like to write contacts.',
        //     buttonPositive: 'Please accept bare mortal',
        // })
        // .then((res) => {
        //     console.log('Permission: ', res);
        //     var newPerson = {
        //         givenName: "Pradafia"
        //     }
              
        //     Contacts.addContact(newPerson);
        // })
        // .catch((error) => {
        //     console.error('Permission error: ', error);
        // });

        var newPerson = {
            givenName: "Alfaryzky"
        }
          
        Contacts.addContact(newPerson);
    }

    return (
        <View>
            <View style={{marginTop: 50}}>
                <Button onPress={readKontak} title='read'></Button>
            </View>
            <View style={{marginTop: 50}}>
                <Button onPress={addKontak} title='add'></Button>
            </View>
        </View>
    );
}

export default Report;

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    textbold: {
        fontWeight: '700',
        fontSize: 20, 
        fontFamily: 'Poppins-Regular',
        color: '#333333'
    },
    textboldcenter: {
        fontWeight: '700',
        fontSize: 22, 
        fontFamily: 'Poppins-Regular', 
        alignSelf: 'center',
        color: '#333333'
    },
    text: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular', 
        color: '#808080'
    },
    textnocenter: {
        fontSize: 16, 
        fontFamily: 'Poppins-Regular',
        color: '#42baf8'
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
});